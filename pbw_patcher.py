#!/usr/bin/env python3

from zipfile import ZipFile
import sys
import os
import os.path
from distutils.dir_util import copy_tree
import shutil
import json
import os.path


# print help
def showHelp():
	print("")
	print("Usage: python3 pbw_patcher.py <filename.pbw>")
	print("Description: This script patches a pebble app (*.pbw) to run on any pebble model")
	print("")


# Patch a pbw file
def patch_pbw(filename):
	# get first cmd line argument = filename to patch
	file_unpack = filename

	#temp dir name = filename without extension
	temp_dir = file_unpack[:-4]

	# extract pbw
	with ZipFile(file_unpack, 'r') as zf:
		zf.extractall(temp_dir)

	# check if chalk folder already exists / no patch needed
	os.chdir(temp_dir)
	if(os.path.exists("chalk")):
		# if chalk folder exits
		os.chdir("..")
		print("No need to patch this app...")
	else:
		# copy basalt folder content to new chalk folder 
		print("Patching Files ...")
		copy_tree("basalt", "chalk")

		# parse appinfo.json and add targetPlatforms ( ... "targetPlatforms": ["aplite", "basalt", "chalk"], ...)
		with open("appinfo.json", "r") as jsonFile:
			data = json.load(jsonFile)
			# detect apps name
			app_name = data["shortName"]
			# change target platform to all models
			data["targetPlatforms"] = ["aplite", "basalt", "chalk"]
		with open("appinfo.json", "w") as jsonFile:
			json.dump(data, jsonFile)

		# pack the new app
		os.chdir("..")
		shutil.make_archive(app_name, 'zip', temp_dir)
		os.rename(app_name + ".zip", app_name + "_patched" + ".pbw")
		print("Patching finished!")	


	# clean up
	print("Cleaning up ...")
	shutil.rmtree(temp_dir)
	print("Finished.")


if __name__ == "__main__":
	if (len(sys.argv) < 2):
		print("ERROR: Please specify a *.pbw file that should be patched.")
		showHelp()
	if (len(sys.argv) == 2):
				if ( "-h" in sys.argv or "--help" in sys.argv ):
					showHelp()

				elif (".pbw" in sys.argv[1]):
					patch_pbw(sys.argv[1])
				else:
					print("ERROR: " + sys.argv[1] + " is not a *.pbw file")
					showHelp()
					
	if (len(sys.argv) > 2):
		print("ERROR: Too many arguments!")
		showHelp()
	
	
 
