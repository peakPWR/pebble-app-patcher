# Pebble App Patcher

Python Script to patch Pebble Apps (*.pbw) to run on any model including Pebble Time Round

**How to use:**
* Place the pebble app in *.pbw format in the same directory as this script
* Run the script by typing `python3 pbw_patcher.py <filename>.pbw` into a command line prompt
* You will find your patched pbw file in the same directory named after the following scheme: `<APPNAME>_patched.pbw`